﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WorldTest {
    class Program {

        private struct Feature {
            public string id;
            public int x;
            public int y;

            public Feature(string[] data) {
                this.id = data[0];
                this.x = int.Parse(data[1]);
                this.y = int.Parse(data[2]);
            }

            public override string ToString() {
                return "(" + id + " " + x + " " + y + ")";
            }
        }

        private class Node {

            private Feature _feature;

            private Node left;
            private Node right;

            public Node(List<Feature> list, Func<List<Feature>, List<Feature>> mainSort, Func<List<Feature>, List<Feature>> secSort) {
                if(list.Count == 0) {
                    throw new System.ArgumentException("The list is empty!");
                }

                if(list.Count > 1) {
                    list = mainSort(list);
                    int medianIndex = list.Count / 2;
                    _feature = list[medianIndex];
                    List<Feature> leftList = list.GetRange(0, medianIndex);
                    left = new Node(leftList, secSort, mainSort);
                    List<Feature> rightList = list.GetRange(medianIndex + 1, list.Count - leftList.Count - 1);
                    right = new Node(rightList, secSort, mainSort);
                } else {
                    _feature = list[0];
                }

            }

            public void Print() {
                Console.WriteLine(_feature.id + " x: " + _feature.x + " y: " + _feature.y);
                left?.Print();
                right?.Print();
            }

            public Feature FindNearestFrom(Feature feature) {
                throw new System.NotImplementedException();
                //if(feature.id == _feature.id) {

                //}

                //if(feature.x < _feature.x) {
                //    left.FindNearestFrom(feature);
                //} else {
                //    right.FindNearestFrom(feature);
                //}
            }


        }

        private class Tree {

            private List<Feature> SortX(List<Feature> list) {
                return list.OrderBy(x => x.x).ToList();
            }

            private List<Feature> SortY(List<Feature> list) {
                return list.OrderBy(y => y.y).ToList();
            }

            private Node root;

            public Tree(List<Feature> list) {
                root = new Node(list, SortX, SortY);
            }

            public void Print() {
                root?.Print();
            }

            public Feature FindNearestFrom(Feature feature) {
                return root.FindNearestFrom(feature);
            }

        }

        static void Main(string[] args) {

            const string path = "Data/problem_small.txt";

            List<Feature> features = new List<Feature>();


            using(StreamReader streamReader = new StreamReader(path)) {
                while(!streamReader.EndOfStream) {
                    string line = streamReader.ReadLine();
                    string[] data = line.Split(' ');
                    Feature feature = new Feature(data);
                    features.Add(feature);
                }
            }

            Tree tree = new Tree(features);

            double maxDistanceSqr = 0;
            int featureIndex = -1;

            for(int i = 0; i < features.Count; ++i) {
                Feature one = features[i];

                Feature nearest = tree.FindNearestFrom(one);

                double distanceSqr = Math.Pow(nearest.x - one.x, 2) + Math.Pow(nearest.y - one.y, 2);

                if(maxDistanceSqr < distanceSqr) {
                    maxDistanceSqr = distanceSqr;
                    featureIndex = i;
                }

            }


            // double minDistance = double.MaxValue;
            //double maxDistance = 0;
            //int featureIndex = -1;
            //for(int i = 0; i < features.Count; ++i) {

            //    double minDistance = double.MaxValue;

            //    for(int j = 0; j < features.Count; ++j) {
            //        Feature one = features[i];
            //        Feature two = features[j];

            //        double distanceSqr = Math.Sqrt(Math.Pow(two.x - one.x, 2) + Math.Pow(two.y - one.y, 2));
            //        //            avg += distanceSqr;
            //        Console.WriteLine("Distance. p" + i + "-p" + j + "=" + distanceSqr);
            //        if(distanceSqr > 0 && minDistance > distanceSqr) {
            //            minDistance = distanceSqr;
            //        }

            //    }

            //    if(maxDistance < minDistance) {
            //        maxDistance = minDistance;
            //        featureIndex = i;
            //    }
            //}




            //Tree tree = new Tree(features);
            //tree.Print();

            if(featureIndex < 0) {
                Console.WriteLine("Didn't find any features. Not possible! Code is wrong!");
            } else {
                Feature feature = features[featureIndex];
                Console.WriteLine("Found feature: " + feature.id);
            }

            Console.WriteLine("Finished execution...");
            Console.ReadLine();
        }



  
    }
}
