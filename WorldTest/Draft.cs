﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace WorldTest {


//    private struct Feature {
//        public string id;
//        public int x;
//        public int y;

//        public Feature(string[] data) {
//            this.id = data[0];
//            this.x = int.Parse(data[1]);
//            this.y = int.Parse(data[2]);
//        }

//        public override string ToString() {
//            return "(" + id + " " + x + " " + y + ")";
//        }
//    }

//    private class PolarTreeLeaf {

//        public PolarTreeLeaf(Feature feature) {
//            xFeature = feature;
//            yFeature = feature;
//        }

//        public Feature xFeature;
//        public Feature yFeature;


//    }

//    //left to right up - y=x formula
//    //left to right down - y=-x+10 formula


//    //this tree works on square map only yet
//    private class PolarTree {

//        private const int quadrantAmount = 8;

//        /*
//         *---*---*
//         | 2 | 3 |
//         *---*---*
//         | 1 | 4 |
//         *---*---*
//        */

//        private readonly int height;

//        private readonly int halfWidth;
//        private readonly int halfHeight;

//        //diagonal up
//        //diagonal down

//        public PolarTree(int width, int height) {
//            this.height = height;

//            this.halfWidth = width / 2;
//            this.halfHeight = height / 2;
//        }

//        private PolarTreeLeaf[] polarTreeLeaves = new PolarTreeLeaf[quadrantAmount];

//        //0, 1, 2, ..., 7

//        //starts at lower left, right half corner

//        public void Add(Feature feature) {
//            bool isLeft = feature.x < halfWidth;
//            if(isLeft) {
//                AddLeft(feature);
//            } else {
//                AddRight(feature);
//            }
//        }

//        private void AddRight(Feature feature) {
//            bool isBottom = feature.y < halfHeight;
//            if(isBottom) {
//                AddBottomRightQuadrant(feature);
//            } else {
//                AddTopRightQuadrant(feature);
//            }
//        }

//        private void AddTopRightQuadrant(Feature feature) {
//            bool isAbove = feature.y > feature.x;
//            if(isAbove) {
//                PolarTreeLeaf leaf = polarTreeLeaves[4];
//                if(leaf == null) {
//                    polarTreeLeaves[4] = new PolarTreeLeaf(feature);
//                } else {
//                    //max
//                    if(leaf.xFeature.x < feature.x) {
//                        leaf.xFeature = feature;
//                    }

//                    //max
//                    if(leaf.yFeature.y < feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            } else {
//                PolarTreeLeaf leaf = polarTreeLeaves[5];
//                if(leaf == null) {
//                    polarTreeLeaves[5] = new PolarTreeLeaf(feature);
//                } else {
//                    //max
//                    if(leaf.xFeature.x < feature.x) {
//                        leaf.xFeature = feature;
//                    }

//                    //max
//                    if(leaf.yFeature.y < feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            }
//        }

//        private void AddBottomRightQuadrant(Feature feature) {
//            bool isAbove = feature.y > -feature.x + height;
//            if(isAbove) {
//                PolarTreeLeaf leaf = polarTreeLeaves[6];
//                if(leaf == null) {
//                    polarTreeLeaves[6] = new PolarTreeLeaf(feature);
//                } else {
//                    //max
//                    if(leaf.xFeature.x < feature.x) {
//                        leaf.xFeature = feature;
//                    }

//                    //min
//                    if(leaf.yFeature.y > feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            } else {
//                PolarTreeLeaf leaf = polarTreeLeaves[7];
//                if(leaf == null) {
//                    polarTreeLeaves[7] = new PolarTreeLeaf(feature);
//                } else {
//                    //max
//                    if(leaf.xFeature.x < feature.x) {
//                        leaf.xFeature = feature;
//                    }

//                    //min
//                    if(leaf.yFeature.y > feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            }
//        }

//        private void AddLeft(Feature feature) {
//            bool isBottom = feature.y < halfHeight;
//            if(isBottom) {
//                AddBottomLeftQuadrant(feature);
//            } else {
//                AddTopLeftQuadrant(feature);
//            }
//        }

//        private void AddTopLeftQuadrant(Feature feature) {
//            bool isAbove = feature.y > -feature.x + height;
//            if(isAbove) {
//                PolarTreeLeaf leaf = polarTreeLeaves[3];
//                if(leaf == null) {
//                    polarTreeLeaves[3] = new PolarTreeLeaf(feature);
//                } else {
//                    //min
//                    if(leaf.xFeature.x > feature.x) {
//                        leaf.xFeature = feature;
//                    }

//                    //max
//                    if(leaf.yFeature.y < feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            } else {
//                PolarTreeLeaf leaf = polarTreeLeaves[2];
//                if(leaf == null) {
//                    polarTreeLeaves[2] = new PolarTreeLeaf(feature);
//                } else {
//                    //min
//                    if(leaf.xFeature.x > feature.x) {
//                        leaf.xFeature = feature;
//                    }
//                    //max
//                    if(leaf.yFeature.y < feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            }
//        }

//        private void AddBottomLeftQuadrant(Feature feature) {
//            bool isAbove = feature.y > feature.x;
//            if(isAbove) {
//                PolarTreeLeaf leaf = polarTreeLeaves[1];
//                if(leaf == null) {
//                    polarTreeLeaves[1] = new PolarTreeLeaf(feature);
//                } else {
//                    //min
//                    if(leaf.xFeature.x > feature.x) {
//                        leaf.xFeature = feature;
//                    }
//                    //min
//                    if(leaf.yFeature.y > feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            } else {
//                PolarTreeLeaf leaf = polarTreeLeaves[0];
//                if(leaf == null) {
//                    polarTreeLeaves[0] = new PolarTreeLeaf(feature);
//                } else {
//                    //min
//                    if(leaf.xFeature.x > feature.x) {
//                        leaf.xFeature = feature;
//                    }
//                    //min
//                    if(leaf.yFeature.y > feature.y) {
//                        leaf.yFeature = feature;
//                    }
//                }
//            }
//        }

//        public void Print() {
//            for(int i = 0; i < polarTreeLeaves.Length; ++i) {
//                PolarTreeLeaf leaf = polarTreeLeaves[i];
//                if(leaf != null) {
//                    Console.WriteLine("Quadrant-" + i + ": x" + leaf.xFeature + " y: " + leaf.xFeature);
//                }
//            }
//        }

//    }

//    private void BruteForceTest() {
//        const string path = "Data/problem_small.txt";

//        List<Feature> features = new List<Feature>();


//        using(StreamReader streamReader = new StreamReader(path)) {
//            while(!streamReader.EndOfStream) {
//                string line = streamReader.ReadLine();
//                string[] data = line.Split(' ');
//                Feature feature = new Feature(data);
//                features.Add(feature);
//            }
//        }

//        double maxDistance = 0;
//        int featureIndex = -1;
//        for(int i = 0; i < features.Count; ++i) {

//            double avg = 0;

//            for(int j = 0; j < features.Count; ++j) {
//                Feature one = features[i];
//                Feature two = features[j];

//                double distanceSqr = Math.Sqrt(Math.Pow(two.x - one.x, 2) + Math.Pow(two.y - one.y, 2));
//                avg += distanceSqr;
//                // Console.WriteLine("Distance. p" + i + "-p" + j + "=" + distanceSqr);
//                //if(maxDistance < distanceSqr) {
//                //    maxDistance = distanceSqr;
//                //    featureIndex = i;
//                //}

//            }

//            if(maxDistance < avg) {
//                maxDistance = avg;
//                featureIndex = i;
//            }
//        }

//        if(featureIndex < 0) {
//            Console.WriteLine("Didn't find any features. Not possible! Code is wrong!");
//        } else {
//            Feature feature = features[featureIndex];
//            Console.WriteLine("Found feature: " + feature.id);
//        }

//        Console.ReadLine();
//    }

//}


//private void CalculatePolarTree() {
//    const string path = "Data/problem_big.txt";

//    const int width = 10000000;
//    const int height = 10000000;
//    PolarTree polarTree = new PolarTree(width, height);

//    using(StreamReader streamReader = new StreamReader(path)) {
//        while(!streamReader.EndOfStream) {
//            string line = streamReader.ReadLine();
//            string[] data = line.Split(' ');
//            Feature feature = new Feature(data);
//            polarTree.Add(feature);
//        }
//    }



//    polarTree.Print();

//    Console.WriteLine("Finished execution...");
//    Console.ReadLine();
//}